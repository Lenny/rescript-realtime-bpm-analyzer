open Audio

let onAnalyzerMessage = (event, bpm, threshold) => {
  Js.log(
    `${event} ${Array.fold_left(
        (acc, val) => acc ++ " " ++ Belt.Int.toString(val),
        "",
        bpm,
      )} ${Belt.Float.toString(threshold)}`,
  )
}

let main = {
  let ctx = AudioContext.create()
  MediaStream.getStream()->Promise.thenResolve(v => {
    switch v {
    | MediaStream.Error(e) => Js.log(e)
    | MediaStream.Ok(stream) =>
      let microphone = ctx->AudioContext.createMediaStreamSource(stream)
      let micCtx = AudioContext.fromAudioNode(microphone)
      let worklet = AudioContext.worklet(micCtx)
      let _ =
        worklet
        ->AudioWorklet.addModule(~moduleURL="realtime-bpm-processor.js")
        ->Promise.thenResolve(_ => {
          let node = AudioWorkletNode.create(
            ~audioContext=micCtx,
            ~processorName="realtime-bpm-processor",
          )
          Js.log(micCtx->AudioContext.sampleRate)
          node
          ->AudioWorkletNode.port
          ->AudioWorkletMessagePort.onmessage(
            e => {
              onAnalyzerMessage(
                e.data.message,
                e.data.result.bpm->Js.Array2.map(item => item.interval),
                e.data.result.threshold,
              )
            },
          )
          AudioWorkletNode.connectToAudioNode(microphone, node)->AudioNode.connect(
            AudioContext.getDestination(micCtx),
          )
        })
    }
  })
}
