type t

@new
external create: (~audioContext: Audio__AudioContext.t, ~processorName: string) => t =
  "AudioWorkletNode"
@get external port: t => Audio__AudioWorkletMessagePort.t = "port"

@send external connectToAudioNode: (Audio__AudioNode.t, t) => Audio__AudioNode.t = "connect"
