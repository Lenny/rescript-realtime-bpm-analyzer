type t

@send external addModule: (t, ~moduleURL: string) => Promise.t<unit> = "addModule"
