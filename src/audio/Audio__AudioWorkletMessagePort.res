type t

type bpmItem = {
  interval: int,
  count: int,
}

type result = {
  bpm: array<bpmItem>,
  threshold: float,
}

type data = {message: string, result: result}
type event = {data: data}

@set external onmessage: (t, event => unit) => unit = "onmessage"
