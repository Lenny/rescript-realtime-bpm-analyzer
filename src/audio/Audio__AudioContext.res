type t

@new external create: unit => t = "AudioContext"
@get external getDestination: t => Audio__AudioNode.t = "destination"
@get external getCurrentTime: t => float = "currentTime"
@get external sampleRate: t => int = "sampleRate"
@get external worklet: t => Audio__AudioWorklet.t = "audioWorklet"
@send external createGain: t => Audio__AudioNode.t = "createGain"
@send external createOscillator: t => Audio__AudioNode.t = "create"
@send
external createMediaElementSource: (t, Dom.element) => Audio__AudioNode.t =
  "createMediaElementSource"
@send
external createMediaStreamSource: (t, Audio__MediaStream.t) => Audio__AudioNode.t =
  "createMediaStreamSource"

// AudioNode
@get external fromAudioNode: Audio__AudioNode.t => t = "context"
