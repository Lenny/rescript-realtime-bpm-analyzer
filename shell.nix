# save this as shell.nix
{pkgs ? import <nixpkgs> {}}:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    python311
    nodejs
    nodePackages.npm
  ];
}
