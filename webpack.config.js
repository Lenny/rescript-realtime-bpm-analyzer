const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const path = require('path');

module.exports = {
    mode: 'none',
    entry: "./src/Main.bs.js",
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'index_bundle.js',
    },
    plugins: [
        new HtmlWebpackPlugin(),
        new CopyPlugin({
            patterns: [
                {
                    from: './node_modules/realtime-bpm-analyzer/dist/realtime-bpm-processor.js',
                    to: path.resolve(__dirname, './dist'),
                },
            ],
        })
    ],
};